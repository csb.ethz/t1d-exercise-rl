## Information

This repository contains source code in Python for the manuscript:

Dana Zimmermann, Hans-Michael Kaltenbach (2023).
_Reducing exercise-related hypoglycemia in automated insulin delivery with reinforcement learning._
IFAC Biomedical Systems 2024

* Corresponding author: Hans-Michael Kaltenbach michael.kaltenbach@bsse.ethz.ch


The simulator and MPC code used in this project is based on code by Julia Deichmann (https://gitlab.com/csb.ethz/t1d-exercise-control/).

### Training of an Actor-Critic Model

To train a Actor-Critic Model, specify the patient that the model should be trained on by setting the variable ID and model name inside the file **run_simulation_RL.py** and then run this file with

```$ python3 run_simulation_RL.py```

### Evaluation of an Actor-Critic Model

After training a model it can be evaluated with the file **evaluation_RL.py**. Before the command below is executed, the ID of the patient and the trained RL model has to be defined and the scenario has to be chosen.

``` $ python3 evaluation_RL.py ```

This will evaluate the model of the chosen patient and the chosen scenario. Further, it will store the resulting blood glucose values in a file (*data/BG_pMPC-RL_patID_sceni.csv*, *data/BG_pMPC-RL_patID_sceni.csv*).

### Analyse Results

If multiple models have been trained, the Jupyter Notebook **analysing.ipynb** summarises the results of these by computing the means of the metrics computed above and plots the blood glucose outcomes.

### Structure

* `data`: Contains generated results from simulations.
* `functions`: Functions required to run simulations.
* `params`: Patient, treatment and process parameters.


## Requirements

* Python (3.7)
* Python packages: numpy (1.19.2), scipy (1.5.0), pandas (1.3.2), matplotlib (3.2.2), quadprog (0.1.11), 
filterpy (1.4.5), torch (2.2.2)

## Author

Dana Zimmermann (zdana@ethz.ch)

## License

Source code licensed under BSD-3-Clause. Copyright © 2024 ETH Zurich, Dana Zimmermann, Hans-Michael Kaltenbach; D-BSSE; CSB Group. For details see LICENSE file.
