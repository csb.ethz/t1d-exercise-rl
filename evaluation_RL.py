# **********************************************************************************************************************
#  Copyright © 2024 ETH Zurich, Dana Zimmermann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-rl
# **********************************************************************************************************************

# File: evaluation_RL.py
# Author: Dana Zimmermann <zdana@ethz.ch>
# Date: 28.04.2024

# Evaluates an Actor-Critic model for a specific patient on a set of different exercise/meal scenarios.


import functions.Simulator as sim


''' define controller type '''

test = True                         # set test = True to evaluate a trained Actor-Critic; default = False


''' set up simulator '''

step_size = 5                       # step size [min]; multiple of 5; default = 5
target = 110                        # blood glucose target concentration [mg/dl]; default = 110

seed = 100                          # seed for CGM noise generator; default = None


''' create simulation scenario '''

sim_dur = 30                        # simulation duration [h]; default = 24

# define meals
t_meals = [6, 12, 18]               # time of meal intake [h]
D = [60, 90, 70]                    # CHO content of meal [g]
tau = [45, 60, 60]                  # time to maximum appearance [min]; default = 60min
announced = [1, 1, 1]               # meal announced: 0 (no) / yes (1); default = 1; vary for incorrect meal size
meal_bolus = [1, 1, 1]              # is meal bolus given: 0 (no) / yes (1); default = 1


''' evaluation/test scenario '''

ID = 100                    # set patient ID to evaluate
model_name = 'ac_100'       # specify which model should be used for evaluation
path = 'data/'              # specify where results should be stored
RL = True                   # set to true, if RL should be used


if RL == True:
    # scenario 1c: moderate-intensity exercise (60min @ 60% VO2max) in the afternoon
    scen1_MPC_RL = sim.Simulator(meal_scen=[t_meals+[14+2/6, 14+4/6, 15], D+[5, 5, 20], tau+[20, 20, 45], announced+[1, 1, 1], meal_bolus+[0, 0, 0]], 
                        PA_scen=[(14, 1, 4317)],
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=True)
    scen1_MPC_RL.run_sim_RL(num_episodes=1, test=True, model_name=model_name)

    # scenario 2c: prolonged moderate-intensity exercise (150min @ 60% VO2max) in the morning
    scen2_MPC_RL = sim.Simulator(meal_scen=[t_meals+[8+25/60, 8+50/60, 9+15/60, 9+40/60, 9+5/60, 10.5], D+[20, 20, 20, 20, 20, 30], tau+[20, 20, 20, 20, 20, 45], announced+[1, 1, 1, 1, 1, 1], meal_bolus+[0, 0, 0, 0, 0, 0]], 
                        PA_scen=[(8, 2.5, 4317)], 
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=True)
    scen2_MPC_RL.run_sim_RL(num_episodes=1, test=True, model_name=model_name)

    # scenario 3c: high-intensity exercise (30min @ 85% VO2max) in the evening
    scen3_MPC_RL = sim.Simulator(meal_scen=[t_meals, D, tau, announced, meal_bolus], 
                        PA_scen=[(19.25, 0.5, 6169)], 
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=True)
    scen3_MPC_RL.run_sim_RL(num_episodes=1, test=True, model_name=model_name)

    print('Patient', ID, 'evaluated')


if RL == False:
    # scenario 1c: moderate-intensity exercise (60min @ 60% VO2max) in the afternoon
    scen1_MPC = sim.Simulator(meal_scen=[t_meals+[14+2/6, 14+4/6, 15], D+[5, 5, 20], tau+[20, 20, 45], announced+[1, 1, 1], meal_bolus+[0, 0, 0]], 
                        PA_scen=[(14, 1, 4317)],
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=False)
    scen1_MPC.run_sim()

    # scenario 2c: prolonged moderate-intensity exercise (150min @ 60% VO2max) in the morning 
    scen2_MPC = sim.Simulator(meal_scen=[t_meals+[8+25/60, 8+50/60, 9+15/60, 9+40/60, 9+5/60, 10.5], D+[20, 20, 20, 20, 20, 30], tau+[20, 20, 20, 20, 20, 45], announced+[1, 1, 1, 1, 1, 1], meal_bolus+[0, 0, 0, 0, 0, 0]], 
                        PA_scen=[(8, 2.5, 4317)], 
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=False)
    scen2_MPC.run_sim()

    # scenario 3c: high-intensity exercise (30min @ 85% VO2max) in the evening
    scen3_MPC = sim.Simulator(meal_scen=[t_meals, D, tau, announced, meal_bolus], 
                        PA_scen=[(19.25, 0.5, 6169)], 
                        sim_dur=sim_dur, 
                        step_size=step_size, 
                        target=target, 
                        path=path,
                        seed=seed, 
                        ID=ID, 
                        SIest=False, 
                        RL=False)
    scen3_MPC.run_sim()