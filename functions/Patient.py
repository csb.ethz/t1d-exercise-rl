# **********************************************************************************************************************
#  Copyright © 2023 ETH Zurich, Julia Deichmann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-control/-/blob/master/LICENSE
# **********************************************************************************************************************

import numpy as np
import pandas as pd
from scipy.integrate import odeint
from collections import namedtuple

Observation = namedtuple("observation", ['BG', 'CGM'])


class T1DPatient:

    def __init__(self, sim_dur=24*60, step_size=5, seed=None):
        """ Create T1D patient.

        :param float sim_dur: Simulation duration [min]; default = 24*60
        :param int step_size: Step size [min]; multiple of 5; default = 5
        :param int seed: Seed for CGM noise generator; default = None
        """

        self.dur = sim_dur
        self.ts = step_size
        self.steps = int(self.dur / self.ts)

        self.seed = seed

        # import population parameters & generate CGM noise
        self.params_pop = pd.read_csv('params/patient_params.csv', sep=';')
        self.noise_pop = self.generate_noise()


    def init_patient(self, patID, init_state=None):
        """ Initialize patient. """

        self.t = 0
        self.step = 0

        # extract patient parameters
        tmp = self.params_pop.iloc[patID]
        self.params = tmp.iloc[15:].to_dict()

        # define initial state if not provided
        if init_state is None:
            init_state = tmp.iloc[:15].to_list()
        self.state = init_state

        # initialize meal rate of appearance
        self.Ra = np.zeros(self.steps)

        # extract noise
        self.noise = self.noise_pop[patID, :]


    def make_step(self, meal, AC, u):
        self.compute_Ra(meal)
        self.state = odeint(self.model, self.state, [0, self.ts],
                            args=(self.params, u, self.Ra[self.step], AC))[1]
        self.step += 1
        self.t += self.ts


    def model(self, x, t, params, u, Ra, AC):
        """ Patient model.

        :param list x: Model state
        :param int t: Time
        :param dict params: Model parameters
        :param float u: Insulin input [muU/min]
        :param float Ra: Meal appearance rate [mg/min]
        :param float AC: Accelerometer count [counts/min]

        :return dxdt
        """

        x1, x2, I, X, Q1, Q2, GI, Y, Z, rGU, rGP, tPA, PAint, rdepl, th = x

        Qb = params['Gb'] * params['Vg']

        fY = (Y / params['aY']) ** params['n1'] / (1 + (Y / params['aY']) ** params['n1'])
        fAC = (AC / params['aAC']) ** params['n2'] / (1 + (AC / params['aAC']) ** params['n2'])
        fHI = (AC / params['aHI']) ** params['n2'] / (1 + (AC / params['aHI']) ** params['n2'])
        fp = (th / params['tHI']) ** params['n2'] / (1 + (th / params['tHI']) ** params['n2'])

        if tPA >= 1:
            t_depl = - params['adepl'] * PAint / tPA + params['bdepl']
        else:
            t_depl = params['bdepl']
        ft = (tPA / t_depl) ** params['n1'] / (1 + (tPA / t_depl) ** params['n1'])

        q3 = (1 - fp) * params['q3l'] + fp * params['q3h']
        q4 = (1 - fp) * params['q4l'] + fp * params['q4h']

        rm = params['beta'] * (q3 / q4 * Y + (1 - params['alpha']) * params['EGP0'] / Qb)

        dxdt = [-params['k1'] * x1 + u,
                params['k1'] * x1 - (params['k2'] + params['k3']) * x2,
                params['k2'] / (params['Vi'] * params['BW']) * x2 - params['k4'] * I,
                params['SI'] * params['p2'] * I - params['p2'] * X,
                -(params['p1'] + rGU - (rGP - rdepl) + (1 + Z) * X + params['p4']) * Q1 + params['p5'] * Q2 +
                params['EGP0'] + Ra / params['BW'],
                params['p4'] * Q1 - params['p5'] * Q2,
                1 / params['tau_G'] * (Q1 / params['Vg'] - GI),
                1 / params['tau_AC'] * (AC - Y),
                params['b'] * fY * Y - 1 / params['tau_Z'] * (1 - fY) * Z,
                params['q1'] * fY * Y - params['q2'] * rGU,
                q3 * fY * Y - q4 * rGP,
                fAC - (1 - fAC) * tPA,
                fAC * AC - (1 - fAC) * PAint,
                params['q6'] * (ft * rm - rdepl),
                fHI - (1 - fHI) * params['q5'] * th]

        return dxdt


    def observation(self):
        Q1 = self.state[4]
        BG = Q1 / self.params['Vg']
        GI = self.state[6]
        CGM = GI + self.noise[self.step]
        observation = Observation(BG=BG, CGM=CGM)
        return observation


    def generate_noise(self):

        if self.seed is not None:
            np.random.seed(self.seed)

        npop = 101
        noise = np.zeros((npop, self.steps))
        w = np.random.randn(npop, self.steps) * np.sqrt(14.45)

        for j in range(npop):
            v = np.zeros(self.steps)
            for i in range(self.steps):
                if i == 0:
                    v[i] = w[j, i]
                elif i == 1:
                    v[i] = 1.013 * v[i - 1] + w[j, i]
                else:
                    v[i] = 1.013 * v[i - 1] - 0.2135 * v[i - 2] + w[j, i]
            noise[j, :] = v

        return noise


    def compute_Ra(self, meal):
        """ Compute meal appearance rate Ra. """

        if meal.CHO > 0:
            D = meal.CHO * 1000
            f = self.params['f']
            tau = meal.tau
            t_tmp = np.arange(0, int(self.dur - self.step * self.ts))
            Ra_tmp = f * D * t_tmp * np.exp(-t_tmp / tau) / tau ** 2
            self.Ra[self.step:] += np.mean(Ra_tmp.reshape(-1, self.ts), axis=1)
