# **********************************************************************************************************************
#  Copyright © 2023 ETH Zurich, Julia Deichmann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-control/-/blob/master/LICENSE
#
#  Modified by Dana Zimmermann
#  Date of last modification: 28.04.2024
#  - Included variable 'RL' in initialization of Simulator
#  - Added function 'run_sim_RL()'
#  - Added function 'simulate_patient_RL()'
# **********************************************************************************************************************

import functions.Patient as patient
import functions.Controller as control
import functions.ActorCritic as ac
from collections import namedtuple
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torch.nn.functional as F
import os

Meal = namedtuple("meal", ['CHO', 'tau', 'announced', 'meal_bolus'])

class Simulator:

    def __init__(self, meal_scen, PA_scen, sim_dur=24, step_size=5, target=110, path=None, seed=None, ID=None,
                 SIest=True, RL=False):
        """ Simulate glucose glucose trajectories for in silico patient population using a model predictive control
        algorithm for insulin delivery informed by insulin sensitivity estimates.

        :param list meal_scen: Meal scenario including lists for
                                            - time of meal intake [h]
                                            - CHO content of meals [g]
                                            - time to maximum appearance, tau [min]; default = 60
                                            - meal announced: 0(no)/1(yes); default=1; vary from 1 to mimic incorrectly
                                                                                       estimated meal sizes
                                            - deliver meal bolus: 0(no)/1(yes); default = 1
        :param list PA_scen: Exercise scenario defined by (exercise start [h], duration [h], intensity [AC])
        :param float sim_dur: Simulation duration [h]; default = 24
        :param int step_size: Step size [min]; multiple of 5; default = 5
        :param float target: Glucose target concentration [mg/dl]; default = 110
        :param str path: Path to save data; default = None (data are not saved)
        :param int seed: Seed for CGM noise generator; default = None
        :param int ID: Patient ID (ID = 0-100); default = None (full population is simulated)
        :param bool SIest: Insulin sensitivity estimation on (True) or off (False); default = True
        :param bool RL: Reinforcement Learning Actor-Critic on (True) or off (False); default = False
        """

        self.dur = int(sim_dur*60)
        self.ts = step_size
        self.steps = int(self.dur / self.ts)
        self.time = np.arange(0, self.dur, self.ts)

        self.target = target

        self.path = path
        self.seed = seed

        self.id = ID
        self.SIest = SIest
        self.RL = RL
        self.PA = PA_scen

        # initialize output arrays
        self.npop = 101
        if self.id is None:
            shape_in = (self.npop, self.steps-1)
            shape_out = (self.npop, self.steps)
        else:
            shape_in = (self.steps-1,)
            shape_out = (self.steps,)

        self.BG = np.zeros(shape_out)
        self.CGM = np.zeros(shape_out)

        self.basal = np.zeros(shape_in)
        self.bolus = np.zeros(shape_in)

        self.us = np.zeros(shape_out)
        self.Si_est = np.zeros(shape_out)
        self.Si_true = np.zeros(shape_out)

        # create simulation scenario
        self.scen = self.create_scenario(meal_scen, PA_scen)


    def run_sim(self, **options):
        """ Run simulations for individual patient or population and save data.

        :param dict, optional options: Dictionary to provide manual inputs:
                                       - 'manual_bolus': Deliver user-defined insulin bolus, [time [h], amount [U],
                                                         announced (0/1)]
                                       - 'SIstep': Change in insulin sensitivity parameter [time [h], factor]
        """

        sim = patient.T1DPatient(sim_dur=self.dur, step_size=self.ts, seed=self.seed)
        ctrl = control.Controller(sim_dur=self.dur, step_size=self.ts, target=self.target, SIest=self.SIest)

        if self.id is None:
            for i in range(self.npop):
                print('Patient ', i+1, '/', self.npop)
                out = self.simulate_patient(patID=i, sim=sim, ctrl=ctrl, **options)
                self.BG[i, :], self.CGM[i, :], self.basal[i, :], self.bolus[i, :], self.us[i, :], self.Si_est[i, :], \
                self.Si_true[i, :] = out

        else:
            out = self.simulate_patient(patID=self.id, sim=sim, ctrl=ctrl, **options)
            self.BG[:], self.CGM[:], self.basal[:], self.bolus[:], self.us[:], self.Si_est[:], self.Si_true[:] = out

        if self.path is not None:
            if self.SIest is True:
                file_ext = 'pMPC-SI'
            else:
                file_ext = 'pMPC'

            if self.id is not None:
                # ensures that the values are written to different files, based on the evaluation scenario
                if self.PA == [(14, 1, 4317)]:
                    file_ext += '_pat' + str(self.id) + '_scen1'
                elif self.PA == [(8, 2.5, 4317)]:
                    file_ext += '_pat' + str(self.id) + '_scen2'
                elif self.PA == [(19.25, 0.5, 6169)]:
                    file_ext += '_pat' + str(self.id) + '_scen3'
                else:
                    file_ext += '_pat' + str(self.id)

            np.savetxt(f'{self.path}/BG_{file_ext}.csv', self.BG, delimiter=';')
            # np.savetxt(f'{self.path}/basal_{file_ext}.csv', self.basal, delimiter=';')


    def simulate_patient(self, patID, sim, ctrl, **options):
        """ Carry out individual patient simulation.

        :return: BG, CGM,
                 controller-delivered basal rate, bolus insulin,
                 target delivery rate,
                 estimated insulin sensitivity, true insulin sensitivity
        """

        # initialize patient and controller
        sim.init_patient(patID)
        ctrl.init_MPC(patID)

        # initialize output
        BG = []
        CGM = []
        obs = sim.observation()
        BG.append(obs.BG)
        CGM.append(obs.CGM)

        basal = []
        bolus = []
        us = [ctrl.us0]

        Si_est = [ctrl.SI0]
        Si_true = [sim.params['SI'] * (1 + sim.state[8])]

        # extract meal and exercise information
        t_meals, meal_params = self.scen[0:2]
        PA = self.scen[2]

        # check for manual bolus injections
        t_bolus = []
        bolus_params = []
        if 'manual_bolus' in options:
            t_bolus.append(int(options['manual_bolus'][0]*60))
            bolus_params.append(options['manual_bolus'][1:])

        # check for artificial SI change
        t_SIstep = []
        factor_SI = []
        if 'SIstep' in options:
            t_SIstep.append(int(options['SIstep'][0] * 60))
            factor_SI.append(options['SIstep'][1])

        # run simulation
        while sim.t < sim.dur - sim.ts:

            # insulin sensitivity change
            if sim.t in t_SIstep:
                idx = t_SIstep.index(sim.t)
                sim.params['SI'] = factor_SI[idx] * sim.params['SI']

            # meals
            if sim.t in t_meals:
                idx = t_meals.index(sim.t)
                meal = Meal(CHO=meal_params[idx][0], tau=meal_params[idx][1], announced=meal_params[idx][2],
                            meal_bolus=meal_params[idx][3])
            else:
                meal = Meal(CHO=0, tau=60, announced=1, meal_bolus=1)

            # exercise
            AC = PA[sim.step]

            # manual bolus injection
            bolus_manual = 0
            if sim.t in t_bolus:
                idx = t_bolus.index(sim.t)
                if bolus_params[idx][1] == 0:
                    bolus_manual = bolus_params[idx][0] * 1e6 / self.ts
                else:
                    ctrl.bolus[sim.step] += bolus_params[idx][0] * 1e6 / self.ts

            # determine controller action
            act = ctrl.compute_input(obs.CGM, sim.step, meal)

            # make simulation step
            sim.make_step(meal=meal, AC=AC, u=act.basal + act.bolus + bolus_manual)
            obs = sim.observation()

            # update controller
            ctrl.update(obs, sim.step-1)

            # append data
            BG.append(obs.BG)
            CGM.append(obs.CGM)

            basal.append(act.basal * 1e-6 * sim.ts)                     # in U/ts
            bolus.append((act.bolus + bolus_manual) * 1e-6 * sim.ts)    # in U

            us.append(ctrl.us)

            Si_est.append(ctrl.SI)
            Si_true.append(sim.params['SI'] * (1 + sim.state[8]))

        return BG, CGM, basal, bolus, us, Si_est, Si_true


    def run_sim_RL(self, num_episodes=1000, test=False, model_name=None, **options):
            """ Run simulations for individual patient or population and save data.

            :param int num_episodes: Specifies how many episodes the model is trained for
            :param bool test: Specifies if the model is being trained or evaluated
            :param dict, optional options: Dictionary to provide manual inputs:
                                        - 'manual_bolus': Deliver user-defined insulin bolus, [time [h], amount [U],
                                                            announced (0/1)]
                                        - 'SIstep': Change in insulin sensitivity parameter [time [h], factor]
            """

            sim = patient.T1DPatient(sim_dur=self.dur, step_size=self.ts, seed=self.seed)
            ctrl = control.Controller(sim_dur=self.dur, step_size=self.ts, target=self.target, SIest=self.SIest)

            # store if training or evaluating and number of episodes
            self.test = test
            self.num_episodes = num_episodes

            # initialize Actor-Critic network & optimizer and set running reward to 0
            model = ac.ActorCritic()
            optimizer = optim.Adam(model.parameters(), lr=0.0001)
            self.running_reward = 0.0

            # load previously trained model for specific patient, if available
            if os.path.exists(f'{model_name}.pth'): 
                checkpoint = torch.load(f'{model_name}.pth')
                model.load_state_dict(checkpoint['model_state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                loss = checkpoint['loss']

            # set model to training or evaluation mode
            if test == False:
                model.train()
            else:
                model.eval()

            # training loop
            for episode in range(self.num_episodes):

                if self.id is None:
                    for i in range(self.npop):
                        out = self.simulate_patient_RL(patID=i, sim=sim, ctrl=ctrl, running_reward=self.running_reward, model=model, optimizer=optimizer, episode=episode, **options)
                        self.BG[i, :], self.CGM[i, :], self.basal[i, :], self.bolus[i, :], self.us[i, :], self.Si_est[i, :], \
                        self.Si_true[i, :], loss = out

                else:
                    out = self.simulate_patient_RL(patID=self.id, sim=sim, ctrl=ctrl, running_reward=self.running_reward,  model=model, optimizer=optimizer, episode=episode, **options)
                    self.BG[:], self.CGM[:], self.basal[:], corrections, self.bolus[:], self.us[:], self.Si_est[:], self.Si_true[:], loss = out

                # store blood glucose values and basal insulin on the last episode of evaluation
                if episode == self.num_episodes-1 and self.test == True and self.path is not None:
                    file_ext = 'pMPC-RL'
                    # ensures that the values are written to different files, based on the evaluation scenario
                    if self.PA == [(14, 1, 4317)]:
                        file_ext += '_pat' + str(self.id) + '_scen1'
                    elif self.PA == [(8, 2.5, 4317)]:
                        file_ext += '_pat' + str(self.id) + '_scen2'
                    elif self.PA == [(19.25, 0.5, 6169)]:
                        file_ext += '_pat' + str(self.id) + '_scen3'
                    else:
                        file_ext += '_pat' + str(self.id)

                    np.savetxt(f'{self.path}/BG_{file_ext}.csv', self.BG, delimiter=';')
                    # np.savetxt(f'{self.path}/basal_{file_ext}.csv', self.basal, delimiter=';')
                    # np.savetxt(f'{self.path}/corrections_{file_ext}.csv', corrections, delimiter=';')

                # print interim values
                if episode % 10 == 0 and self.test == False:
                    print(f"Patient {self.id}, Episode {episode}")

            # save the model
            if self.test == False:
                torch.save({
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': loss,
                }, f'{model_name}.pth')


    def simulate_patient_RL(self, patID, sim, ctrl, model, optimizer, episode, **options):
        """ Carry out individual patient simulation.

        :return: BG, CGM,
                 controller-delivered basal rate, bolus insulin,
                 target delivery rate,
                 estimated insulin sensitivity, true insulin sensitivity,
                 training loss
        """

        # initialisations
        adjustment = [-0.5, 0.0] # adjust basal insulin by decreasing 50% or 0%
        current_reward = 0.0 # reward of the current scenario, episode and patient
        corrections = [] # stores what correction is chosen at a specific time-step
        corrected_insulin = [0.0] # stores the basal insulin with the correction
        action_probs = [] # stores the action probabilities (output from Actor-Critic)
        state_values = [] # stores the estimated state values (output from Actor-Critic)
        recent_PA = 0.0 # set to 1.0 if physical activity is detected

        if self.test == True:
            epsilon = 0.0 # no exploration in case of evaluation
        else: 
            epsilon = 0.05 # for exploration using epsilon-greedy strategy
        
        BG_input_ind = [-19, -16, -13, -10, -7, -4, -1] # indices for BG values from the last 90 min

        # initialize patient and controller
        sim.init_patient(patID)
        ctrl.init_MPC(patID)

        # initialize output
        BG = []
        CGM = []
        obs = sim.observation()
        BG.append(obs.BG)
        CGM.append(obs.CGM)

        basal = []
        bolus = []
        us = [ctrl.us0]

        Si_est = [ctrl.SI0]
        Si_true = [sim.params['SI'] * (1 + sim.state[8])]

        # extract meal and exercise information
        t_meals, meal_params = self.scen[0:2]
        PA = self.scen[2]

        # check for manual bolus injections
        t_bolus = []
        bolus_params = []
        if 'manual_bolus' in options:
            t_bolus.append(int(options['manual_bolus'][0]*60))
            bolus_params.append(options['manual_bolus'][1:])

        # check for artificial SI change
        t_SIstep = []
        factor_SI = []
        if 'SIstep' in options:
            t_SIstep.append(int(options['SIstep'][0] * 60))
            factor_SI.append(options['SIstep'][1])

        # run simulation
        while sim.t < sim.dur - sim.ts:
            # insulin sensitivity change
            if sim.t in t_SIstep:
                idx = t_SIstep.index(sim.t)
                sim.params['SI'] = factor_SI[idx] * sim.params['SI']

            # meals
            if sim.t in t_meals:
                idx = t_meals.index(sim.t)
                meal = Meal(CHO=meal_params[idx][0], tau=meal_params[idx][1], announced=meal_params[idx][2],
                            meal_bolus=meal_params[idx][3])
            else:
                meal = Meal(CHO=0, tau=60, announced=1, meal_bolus=1)

            # exercise
            AC = PA[sim.step]
            # check if there is/has been exercise
            if recent_PA > 0.0:
                recent_PA *= 0.9 # if there has been exercise, reduce "forgetting factor"
            if AC != 0: 
                recent_PA = 1.0 # if there currently is physical activity, set factor to 1.0

            # manual bolus injection
            bolus_manual = 0
            if sim.t in t_bolus:
                idx = t_bolus.index(sim.t)
                if bolus_params[idx][1] == 0:
                    bolus_manual = bolus_params[idx][0] * 1e6 / self.ts
                else:
                    ctrl.bolus[sim.step] += bolus_params[idx][0] * 1e6 / self.ts

            # determine controller action
            act = ctrl.compute_input(obs.CGM, sim.step, meal)
        
            # BG values from the last 90 min in 15 min increments
            BG_input = []
            for ind in BG_input_ind:
                if -len(CGM) <= ind:
                    BG_input.append(CGM[ind])
                else:
                    # if the index is out of bounds, use the most recent BG value
                    BG_input.append(CGM[-1])
            # most recent BG values, last corrected insulin action, current basal insulin action, accelerometer count
            input = torch.tensor(BG_input + [corrected_insulin[-1], act.basal, AC], dtype=torch.float32)
            
            # normalise all input values
            normalized_input = (input - input.mean()) / input.std()
            action_prob, state_value = model(normalized_input) # network returns action probabilities and state value

            # store action probabilities in a list
            state_values.append(state_value.item())
            action_probs.append(action_prob.tolist())

            # create Categorical distribution
            action_distribution = torch.distributions.Categorical(action_prob)

            # epsilon-greedy exploration
            if np.random.rand() < epsilon:
                # explore: choose a random action
                action = torch.tensor(np.random.choice(len(adjustment)))
            else:
                # exploit: choose the action with the highest probability
                action = torch.argmax(action_prob)

            # apply logarithm to the action probabilities
            log_prob = action_distribution.log_prob(action)

            # store log-probabilities and state value (needed for reward computation)
            model.saved_actions.append(log_prob)
            model.state_values.append(state_value)

            # correct the MPC basal insulin with the chosen action from the Actor-Critic
            correction = act.basal * (1 + adjustment[action.item()])

            # store the chosen correction and the corrected insulin
            corrections.append(adjustment[action.item()])
            corrected_insulin.append(correction)

            # make simulation step
            u = correction + act.bolus + bolus_manual
            sim.make_step(meal=meal, AC=AC, u=u)
            obs = sim.observation()

            # update controller
            ctrl.update(obs, sim.step-1)

            # append data
            BG.append(obs.BG)
            CGM.append(obs.CGM)

            basal.append(correction * 1e-6 * sim.ts) # in U/ts
            bolus.append((act.bolus + bolus_manual) * 1e-6 * sim.ts) # in U

            us.append(ctrl.us)

            Si_est.append(ctrl.SI)
            Si_true.append(sim.params['SI'] * (1 + sim.state[8]))

            # compute and store reward and add it to the reward of the current episode
            reward = ac.compute_reward(CGM[-2:], recent_PA, corrections[-1])
            model.rewards.append(reward)
            current_reward += reward

        # add current reward to the running reward
        self.running_reward += current_reward

        # update ActorCritic network & compute loss if the Actor-Critic is being trained
        if self.test == False:
            optimizer.zero_grad()
            loss = ac.compute_loss(model.state_values, model.saved_actions, model.rewards)
            loss.backward()
            optimizer.step()
        else:
            loss = 0.0 

        # plot some information (every 100 episodes)
        if episode % 100 == 0:
            fig, axs = plt.subplots(6, 1, figsize=(12, 7))

            axs[0].plot(BG, label='Blood Glucose')
            axs[0].fill_between(range(len(BG)), 0, 70, color='gray', alpha=0.3)
            axs[0].fill_between(range(len(BG)), 180, 250, color='gray', alpha=0.3)
            axs[0].hlines(110, 0, len(BG), linewidth=0.5, color='red')
            axs[0].set_ylabel('Blood Glucose')

            axs[1].plot(basal, label='Basal Insulin Delivery')
            axs[1].set_ylabel('Basal Insulin')

            axs[2].scatter(range(len(corrections)), corrections, label='RL adjustments', s=0.75)
            axs[2].set_ylim(-0.3, 0.3)
            axs[2].set_ylabel('Corrections')

            axs[3].plot(model.rewards, label='Rewards')
            axs[3].set_ylabel('Rewards')

            axs[4].scatter(range(len(PA)), PA, label='AC', s=0.75)
            axs[4].set_ylabel('Physical Activity')

            axs[5].scatter(range(len(corrections)), [probs[0] for probs in action_probs], label='Action probability -50%', s=0.5)
            axs[5].scatter(range(len(corrections)), [probs[1] for probs in action_probs], label='Action probability +/-0%', s=0.5)
            axs[5].plot(state_values, label='state values', c="green")
            axs[5].set_ylim(-1.1, 1.1)
            axs[5].set_ylabel('Action Probabilities & State Values')
            axs[5].legend()

            fig.suptitle(f'Patient: {patID}')
            plt.show()

        # reset buffers
        model.saved_actions = []
        model.rewards = []
        model.state_values = []

        return BG, CGM, basal, corrections, bolus, us, Si_est, Si_true, loss


    def create_scenario(self, meal, PA):
        """ Create input scenario from meal and exercise information.

        :return time of meals, meal parameters, AC input
        """

        t_meals = [int(np.round(i*60)) for i in meal[0]]
        n = len(t_meals)
        if len(meal[2]) == 0:
            meal[2] = [60] * n
        if len(meal[3]) == 0:
            meal[3] = [1] * n
        if len(meal[4]) == 0:
            meal[4] = [1] * n
        meals = [(meal[1][i], meal[2][i], meal[3][i], meal[4][i]) for i in range(n)]

        AC = np.zeros(self.steps)
        for i in range(len(PA)):
            AC[int(PA[i][0]*60/self.ts):int((PA[i][0]+PA[i][1])*60/self.ts)] = PA[i][2]

        return [t_meals, meals, AC]
