# **********************************************************************************************************************
#  Copyright © 2024 ETH Zurich, Dana Zimmermann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-rl
# **********************************************************************************************************************

# File: ActorCritic.py
# Author: Dana Zimmermann <zdana@ethz.ch>
# Date: 28.04.2024

# Contains the implementation of the Actor-Critic class, as well as functions 
# to compute the loss and the reward and to normalize given values.

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt

torch.manual_seed(44)

class ActorCritic(nn.Module):
    """
    implements both actor and critic in one model
    """
    def __init__(self):
        super(ActorCritic, self).__init__()
        # input dimension: last BG values, corrected MPC insulin action, MPC insulin action, accelerometer count
        self.linear_layer = nn.Linear(10, 128)

        # actor's layer
        # output dimension: 2 basal insulin adjustments (+50%, +/-0%)
        self.action_layer = nn.Linear(128, 2)

        # critic's layer
        # output dimension: 1 state value
        self.value_layer = nn.Linear(128, 1)

        # action, reward & state values buffer
        self.saved_actions = []
        self.rewards = []
        self.state_values = []

    def forward(self, x):
        """
        forward of both actor and critic
        """
        x = F.relu(self.linear_layer(x)) 

        # actor: choses action to take by returning probability of actions -50% and +/-0%
        action_prob = F.softmax(self.action_layer(x), dim=0) # softmax activation function: maps to values [0,1]

        # critic: evaluates being in the current state
        state_value = self.value_layer(x)

        # return values for both actor and critic as a tuple of 2 values:
        # a list with the probability of each action over the action space and the value from current state
        return action_prob, state_value


def compute_reward(BG, recent_PA, correction):
    """
    computes the reward for an action based on recent physical activity and blood glucose values
    """

    reward = 0
    if correction == 0: 
        if BG[1] > 110: # check if BG is above target value
            reward += 3
        if BG[1] > 140: # check if BG is above target range
            reward += 4
        if BG[1] > BG[0]: # check if BG is increasing
            reward += 4
    else: # correction == -0.2
        if BG[1] < 110: # check if BG is below target value
            reward += 3
        if BG[1] < 80: # check if BG is below target range
            reward += 4
        if BG[1] < BG[0]: # check if BG is decreasing
            reward += 2
        if recent_PA > 0.2: # check if there has been PA recently
            reward += 4

    return reward


def compute_loss(critic_values, saved_actions, rewards):
    """
    computes the current loss of the Actor-Critic network

    :param list[float] critic_values: stores state values from 1 episode at every time step
    :param list[tensor] saved_actions: stores log probabilities of actions at every time step during 1 ep
    :param list[int] rewards: stores rewards collected at every time step
    """

    # compute and normalize advantages
    # informs about the extra reward that could be obtained by taking a particular action
    advantages = normalization(torch.tensor(rewards) - torch.tensor(critic_values))

    # compute individual losses
    # neg log prob of the chosen actions weighted by the advantages
    actor_loss = -torch.mean(torch.stack(saved_actions) * advantages)
    critic_loss = F.smooth_l1_loss(torch.tensor(rewards), torch.tensor(critic_values))

    # compute combined loss
    loss = (actor_loss + critic_loss) / 2.0

    return loss


def normalization(values):
    """
    normalizes the values such that the mean of all values is 0 and the std is 1
    """
    
    mean = torch.mean(values)
    std = torch.std(values)

    scaled_values = (values - mean) / std

    return scaled_values
