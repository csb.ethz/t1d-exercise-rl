# **********************************************************************************************************************
#  Copyright © 2023 ETH Zurich, Julia Deichmann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-control/-/blob/master/LICENSE
# **********************************************************************************************************************

import numpy as np
import pandas as pd
from quadprog import solve_qp
from filterpy.kalman import UnscentedKalmanFilter
from filterpy.kalman.sigma_points import JulierSigmaPoints
from collections import namedtuple
import sys

Action = namedtuple("action", ['basal', 'bolus'])


class Controller:

    def __init__(self, sim_dur=24*60, step_size=5, target=110, SIest=True):
        """ MPC controller for insulin delivery informed by insulin sensitivity estimation.

        :param float sim_dur: Simulation duration [min]; default = 24*60
        :param int step_size: Step size [min]; multiple of 5; default = 5
        :param float target: Glucose target concentration [mg/dl]; default = 110
        :param bool SIest: Insulin sensitivity estimation on (True) or off (False); default = True
        """

        self.dur = sim_dur
        self.ts = step_size
        self.steps = int(self.dur / self.ts)

        self.target = target

        self.SIest = SIest

        # import process parameters
        self.params_pop = pd.read_csv('params/process_params.csv', sep=';')
        # import treatment parameters
        self.trt_params_pop = pd.read_csv('params/params_treatment.csv', sep=';')

        # import IOB curves
        self.IOBcurves = np.genfromtxt('params/IOBcurves.csv', delimiter=';')
        if self.ts%5==0:
            self.IOBcurves = self.IOBcurves[:, ::self.ts//5]
        else:
            print('ValueError: Step size must be multiple of 5 to compute IOB constraint.')
            sys.exit()

        # prediction and control horizon
        self.N = int(60/self.ts)
        self.Nc = int(30/self.ts)

        # state and input weights for objective function
        self.Q = np.eye(1)
        self.R = 1e-7 * np.eye(1)

        # rewrite to solve problem as QP
        self.Hbar = np.pad(np.kron(np.eye(self.N), self.Q), ((0, self.Nc), (0, self.Nc))) \
                    + np.pad(np.kron(np.eye(self.Nc), self.R), ((self.N, 0), (self.N, 0)))
        self.q = np.zeros((self.Hbar.shape[1],))


    def init_MPC(self, patID):
        """ Initialize controller. """

        # extract process and treatment parameters
        tmp = self.params_pop.iloc[patID]
        self.params = tmp.iloc[7:].to_dict()
        self.trt_params = self.trt_params_pop.iloc[patID].to_dict()

        # define steady states
        self.xs = np.array([tmp.iloc[:7]]).T
        self.ys = np.array([[self.target]])
        self.us = self.trt_params['ub']

        # baseline us and SI
        self.us0 = self.us
        self.SI0 = self.params['SI']

        self.SI = self.SI0

        # initialize control input delta u
        self.du = np.array([])
        # insulin carry over
        self.u_carry = 0
        # maximum delivery rate
        self.u_max = 20*1e6

        # initial delta x
        self.dx0 = np.zeros(self.xs.shape)

        # system matrices
        system = self.system_matrices(self.params)
        self.A, self.B, self.C, self.D = system
        self.Geq, self.Eeq, self.Deq, self.Gin = self.constraints(system)

        # pre-defined inputs
        self.Ra = np.zeros(self.steps + self.N)
        self.bolus = np.zeros(self.steps + self.N)

        # define observer parameters
        self.obs_params = self.params.copy()
        del self.obs_params['SI']

        # initialize UKF for state estimation
        x0_UKF = list(self.xs.reshape(self.xs.shape[0], )) + [self.params['SI']]
        self.kf = UnscentedKalmanFilter(dim_x=8, dim_z=1, dt=self.ts, fx=self.observer_model, hx=self.hx,
                                   points=JulierSigmaPoints(n=8, kappa=1))
        self.kf.x = x0_UKF

        # initialize covariance, process noise and measurement noise matrices
        self.kf.P *= (np.array([8511, 5833, 1.7, 2.07e-3, 14.2, 9.3, 11, 1.22e-4])) ** 2
        self.kf.Q = np.diag((np.array([851, 583, 0.17, 2.07e-4, 1.42, 0.93, 1.1, 1.22e-5])) ** 2)
        z_std = 7
        self.kf.R = np.diag([z_std ** 2])


    def compute_input(self, observation, step, meal):
        """ Determine insulin input.

        :param float observation: Glucose measurement [mg/dl]
        :param int step: Current time step
        :param namedtuple meal: Current meal input that provides information on meal size 'CHO', time to max appearance
                                'tau', whether meal is 'announced', and whether 'meal_bolus' is delivered

        :return action (basal and bolus inputs)
        """

        self.dx0 = np.array([self.kf.x[:-1]]).T - self.xs

        # compute Ra and bolus inputs
        if meal.CHO * meal.announced > 0:
            self.compute_Ra(meal, step)

            if meal.meal_bolus == 1:
                um = np.round(self.SI0 / self.SI * (meal.CHO * meal.announced / self.trt_params['ICR']
                                                    + (observation - self.ys) / self.trt_params['CF']), 1) * 1e6 \
                     / self.ts
                self.bolus[step] += max(0, um)

        # equality constraints
        input = np.concatenate((self.bolus[step:step+self.N].reshape(self.N,1),
                                self.Ra[step:step+self.N].reshape(self.N,1)), axis=1).flatten().reshape(2*self.N, 1)
        weq = (np.dot(self.Eeq, self.dx0) + np.dot(self.Deq, input))

        # inequality constraints
        uIOBmax = self.compute_IOB(observation, step)
        bu = np.array([[self.u_max-self.us],
                       [self.us],
                       [uIOBmax]])
        win = np.tile(bu, (self.Nc, 1))

        # solve QP
        G = np.vstack([self.Geq, -self.Gin])
        b = np.vstack([weq, -win]).reshape(G.shape[0], )
        u_sol = solve_qp(2 * self.Hbar, self.q, G.T, b, meq=self.Geq.shape[0])[0]

        # retrieve u0
        self.u_hat = u_sol[self.N*self.C.shape[0]:] + self.us
        u0 = u_sol[self.N*self.C.shape[0]]

        # discretize control input
        ui = self.us + u0 + self.u_carry
        u = 10000 * np.floor(ui / 10000)
        self.du = np.append(self.du, u-self.us)
        self.u_carry = ui - u

        action = Action(basal=u, bolus=self.bolus[step])
        return action


    def update(self, observation, step):
        """ Update state estimate (UKF) and use insulin sensitivity estimate to update MPC problem. """

        # UKF: prediction from step --> step+1 based on observer model
        self.kf.predict(params=self.obs_params, ub=self.du[step] + self.us, u=self.bolus[step], Ra=self.Ra[step])
        # UKF: update based on measurement step+1
        self.kf.update(observation.CGM)

        # MPC: update based on new SI estimate
        if self.SIest is True:
            # constrain insulin sensitivity estimation
            if self.kf.x[-1] < 0.5 * self.SI0:
                self.kf.x[-1] = 0.5 * self.SI0
            self.SI = self.kf.x[-1]

            # update target insulin delivery rate
            self.us = self.SI0 / self.SI * self.us0

            # update steady state
            self.xs[0:3, :] = np.array([[self.us / self.obs_params['k1'], self.us / (self.obs_params['k2'] + self.obs_params['k3']),
                                         self.SI0 / self.SI * self.obs_params['Ib']]]).T

            # update system and QP matrices
            self.A[3, 2] = np.eye(7)[3, 2] + self.ts * self.SI * self.obs_params['p2']
            self.Geq, self.Eeq, self.Deq, self.Gin = self.constraints([self.A, self.B, self.C, self.D])

        else:
            self.kf.x[-1] = self.SI0


    def system_matrices(self, params):
        """ Define system matrices to describe process model. """

        Xb = self.SI * params['Ib']
        Qb = params['Gb'] * params['Vg']

        A0 = np.array([[-params['k1'], 0, 0, 0, 0, 0, 0],
                       [params['k1'], -(params['k2'] + params['k3']), 0, 0, 0, 0, 0],
                       [0, params['k2'] / (params['Vi'] * params['BW']), -params['k4'], 0, 0, 0, 0],
                       [0, 0, self.SI * params['p2'], -params['p2'], 0, 0, 0],
                       [0, 0, 0, -Qb, -(params['p1'] + Xb + params['p4']), params['p5'], 0],
                       [0, 0, 0, 0, params['p4'], -params['p5'], 0],
                       [0, 0, 0, 0, 1 / params['tau_G'] / params['Vg'], 0, -1 / params['tau_G']]])
        A = np.eye(len(A0)) + self.ts * A0

        B = self.ts * np.array([[1],
                                [0],
                                [0],
                                [0],
                                [0],
                                [0],
                                [0]])

        C = np.array([[0, 0, 0, 0, 0, 0, 1]])

        D = np.zeros((len(A0), 2))
        D[0, 0] = self.ts * 1
        D[4, 1] = self.ts * 1

        return [A, B, C, D]


    def constraints(self, system_matrices):
        """ Define equality and inequality constraints of QP. """

        A, B, C, D = system_matrices

        dim_x = A.shape[1]
        dim_u = B.shape[1]
        dim_y = C.shape[0]
        dim_d = D.shape[1]

        # equality constraints
        Geq = np.zeros((self.N * dim_y, self.N * dim_u))
        Deq = np.zeros((self.N * dim_y, self.N * dim_d))
        Eeq = np.array([], dtype=np.int64).reshape(0, dim_x)
        for i in range(self.N):
            Geq = Geq + np.kron(np.diag(np.ones(self.N - i), -i),
                                -np.linalg.multi_dot([C, np.linalg.matrix_power(A, i), B]))
            Deq = Deq + np.kron(np.diag(np.ones(self.N - i), -i),
                                np.linalg.multi_dot([C, np.linalg.matrix_power(A, i), D]))
            Eeq = np.concatenate((Eeq, C.dot(np.linalg.matrix_power(A, i + 1))), axis=0)
        Geq = np.concatenate((np.eye(self.N * dim_y), Geq[:, :self.Nc * dim_u]), axis=1)

        # inequality constraints
        Au = np.array([[1],
                       [-1],
                       [1]])
        Gin = np.kron(np.eye(self.Nc), Au)
        Gin = np.concatenate((np.zeros((self.Nc * Au.shape[0], self.N * dim_u)), Gin), axis=1)

        return [Geq, Eeq, Deq, Gin]


    def observer_model(self, x, dt, params, ub, u, Ra):
        """ Observer model for state estimation. """

        x1, x2, I, X, Q1, Q2, GI, SI = x

        F = np.array([x1 + dt * (u + ub - params['k1'] * x1),
                      x2 + dt * (params['k1'] * x1 - (params['k2'] + params['k3']) * x2),
                      I + dt * (params['k2'] / (params['Vi'] * params['BW']) * x2 + - params['k4'] * I),
                      X + dt * (SI * params['p2'] * I - params['p2'] * X),
                      Q1 + dt * (params['EGP0'] + Ra + params['p5'] * Q2 - (params['p1'] + X + params['p4']) * Q1),
                      Q2 + dt * (params['p4'] * Q1 - params['p5'] * Q2),
                      GI + dt * 1 / params['tau_G'] * (Q1 / params['Vg'] - GI),
                      SI], dtype=float)

        return F


    def hx(self, x):
        """ Measurement function for state estimation. """

        return np.array([x[6]])


    def compute_Ra(self, meal, step):
        """ Compute meal appearance rate Ra. """

        D = meal.CHO * meal.announced * 1000
        f = self.params['f']
        tau = meal.tau
        t_tmp = np.arange(0, int(self.dur + (self.N - step) * self.ts))
        Ra_tmp = f * D * t_tmp * np.exp(-t_tmp / tau) / tau ** 2 / self.params['BW']
        self.Ra[step:] += np.mean(Ra_tmp.reshape(-1, self.ts), axis=1)


    def compute_IOB(self, observation, step):
        """ Compute IOB constraint from current glucose measurement and IOB from previous injections. """

        basal_past = np.flip(self.du[max(0, int(step-480 / self.ts)):step])
        bolus_past = np.flip(self.bolus[max(0, int(step+1-480 / self.ts)):step+1])

        if len(basal_past) < int(480/self.ts):
            diff = int(480 / self.ts - len(basal_past))
            basal_past = np.append(basal_past, np.zeros(diff))
            bolus_past = np.append(bolus_past, np.zeros(diff - 1))

        theta_bolus = self.IOBcurves[2, :]
        if observation > 300:
            theta_basal = self.IOBcurves[0, :]
        elif (observation > 200) & (observation <= 300):
            theta_basal = self.IOBcurves[2, :]
        elif (observation > 140) & (observation <= 200):
            theta_basal = self.IOBcurves[4, :]
        else:
            theta_basal = self.IOBcurves[6, :]
        IOBreq = (observation - self.ys) / self.trt_params['CF'] / self.ts * 1e6 * self.SI0 / self.SI
        IOB = np.sum(theta_basal * basal_past + theta_bolus * bolus_past)
        if IOBreq > IOB:
            uIOBmax = (IOBreq - IOB)[0, 0]
        else:
            uIOBmax = 0

        return uIOBmax
