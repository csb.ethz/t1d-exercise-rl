# **********************************************************************************************************************
#  Copyright © 2024 ETH Zurich, Dana Zimmermann, Hans-Michael Kaltenbach; D-BSSE; CSB Group
#  All rights reserved. This program and the accompanying materials are made available under the terms of the BSD-3
#  Clause License which accompanies this distribution, and is available at
#  https://gitlab.com/csb.ethz/t1d-exercise-rl
# **********************************************************************************************************************

# File: run_simulation_RL.py
# Author: Dana Zimmermann <zdana@ethz.ch>
# Date: 28.04.2024

# Trains an Actor-Critic model for a specific patient on a set of different exercise/meal scenarios.

import functions.Simulator as sim

path = None


''' define controller type '''

SIest = False                       # run insulin sensitivity-informed MPC (pMPC-SI)
                                    # set to SIest = False to run baseline MPC (pMPC); default = True

RL = True                           # run MPC with Actor-Critic Reinforcement Learning; default = False

test = False                        # set test = False to train the network; default = False


''' set up simulator '''

step_size = 5                       # step size [min]; multiple of 5; default = 5
target = 110                        # blood glucose target concentration [mg/dl]; default = 110

seed = 100                          # seed for CGM noise generator; default = None

ID = 100                              # set patient ID to simulate individual patient (ID = 0-100), otherwise full
                                    # population is simulated; default=None
                                    # for RL: if set to None, one model is trained for all patients


''' create simulation scenario '''

sim_dur = 30                        # simulation duration [h]; default = 24

# define meals
t_meals = [6, 12, 18]               # time of meal intake [h]
D = [60, 90, 70]                    # CHO content of meal [g]
tau = [45, 60, 60]                  # time to maximum appearance [min]; default = 60min
announced = [1, 1, 1]               # meal announced: 0 (no) / yes (1); default = 1; vary for incorrect meal size
meal_bolus = [1, 1, 1]              # is meal bolus given: 0 (no) / yes (1); default = 1


''' run simulation '''
num_episodes = 5000               # number of learning episodes
model_name = 'ac_100'             # specify the name of the RL model / which pretrained RL model should be used

# scenario 1: moderate-intensity exercise (60min @ 60% VO2max)
#          a: in morning
#          b: in evening

print("------ Scenario 1a -------")

scen1a = sim.Simulator(meal_scen=[t_meals+[9+2/6, 9+4/6, 10], D+[5, 5, 20], tau+[20, 20, 45], announced+[1, 1, 1], meal_bolus+[0, 0, 0]], 
                     PA_scen=[(9, 1, 4317)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen1a.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)

print("------ Scenario 1b -------")

scen1b = sim.Simulator(meal_scen=[t_meals+[19.5+2/6, 19.5+4/6, 20.5], D+[5, 5, 20], tau+[20, 20, 45], announced+[1, 1, 1], meal_bolus+[0, 0, 0]], 
                     PA_scen=[(19.5, 1, 4317)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen1b.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)

# scenario 2: prolonged moderate-intensity exercise (150min @ 60% VO2max)
#          a: in afternoon
#          b: in evening

print("------ Scenario 2a -------")

scen2a = sim.Simulator(meal_scen=[t_meals+[14+25/60, 14+50/60, 15+15/60, 15+40/60, 16+5/60, 16.5], D+[20, 20, 20, 20, 20, 30], tau+[20, 20, 20, 20, 20, 45], announced+[1, 1, 1, 1, 1, 1], meal_bolus+[0, 0, 0, 0, 0, 0]], 
                     PA_scen=[(14, 2.5, 4317)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen2a.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)

print("------ Scenario 2b -------")

scen2b = sim.Simulator(meal_scen=[t_meals+[18+25/60, 18+50/60, 19+15/60, 19+40/60, 19+5/60, 20.5], D+[20, 20, 20, 20, 20, 30], tau+[20, 20, 20, 20, 20, 45], announced+[1, 1, 1, 1, 1, 1], meal_bolus+[0, 0, 0, 0, 0, 0]], 
                     PA_scen=[(18, 2.5, 4317)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen2b.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)

# scenario 3: high-intensity exercise (30min @ 85% VO2max)
#          a: in morning
#          b: in afternoon

print("------ Scenario 3a -------")

scen3a = sim.Simulator(meal_scen=[t_meals, D, tau, announced, meal_bolus], 
                     PA_scen=[(9.25, 0.5, 6169)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen3a.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)

print("------ Scenario 3b -------")

scen3b = sim.Simulator(meal_scen=[t_meals, D, tau, announced, meal_bolus], 
                     PA_scen=[(15.25, 0.5, 6169)], 
                     sim_dur=sim_dur, 
                     step_size=step_size, 
                     target=target, 
                     path=path,
                     seed=seed, 
                     ID=ID, 
                     SIest=SIest, 
                     RL=RL)
scen3b.run_sim_RL(num_episodes=num_episodes, test=test, model_name=model_name)